package com.example.springboottesting.service;

import com.example.springboottesting.model.Employee;

import java.util.List;
import java.util.Optional;
import java.util.stream.DoubleStream;

public interface EmployeeService {
    Employee saveEmployee(Employee employee);
    List<Employee> findAllEmployees();
    Employee updateEmployee(Employee employee);
    void deleteEmploy(long id);
    Optional<Employee> findById(long id);
}
