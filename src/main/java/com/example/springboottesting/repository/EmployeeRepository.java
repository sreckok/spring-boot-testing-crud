package com.example.springboottesting.repository;

import com.example.springboottesting.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findByEmail(String email);

    @Query("select e from Employee e where e.firstName = :firstName and e.lastName = :lastName")
    Employee findByJPQL(@Param("firstName") String firstName, @Param("lastName") String lastName);

    @Query(value = "select * from employees e " +
            "where e.first_name = :firstName " +
            "and e.last_name = :lastName",
            nativeQuery = true)
    Employee findByNativeSql(String firstName, String lastName);
}
