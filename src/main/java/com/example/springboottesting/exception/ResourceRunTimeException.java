package com.example.springboottesting.exception;

public class ResourceRunTimeException extends RuntimeException {
    public ResourceRunTimeException(String message) {
        super(message);
    }

    public ResourceRunTimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
