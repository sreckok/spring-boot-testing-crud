package com.example.springboottesting.service;

import com.example.springboottesting.exception.ResourceRunTimeException;
import com.example.springboottesting.model.Employee;
import com.example.springboottesting.repository.EmployeeRepository;
import com.example.springboottesting.service.impl.EmployeeServiceImpl;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.BDDMockito.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {
    @Mock
    private EmployeeRepository employeeRepository;
    private EmployeeService employeeService;
    private Employee employee;

    @BeforeEach
    public void setup() {
        employee = Employee.builder()
                .id(1L)
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();
        employeeService = new EmployeeServiceImpl(employeeRepository);
    }

    // JUnit test for save Employee method
    @Test
    public void givenEmployeeObject_whenSaveEmployee_thanReturnEmployeeObject() {
        // given - precondition or state
        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.empty());
        when(employeeRepository.save(employee)).thenReturn(employee);

        // when - action or behavior that we are going test
        Employee savedEmployee = employeeService.saveEmployee(employee);

        // than - verify the output
        assertNotNull(savedEmployee, "Employee object should be not null");

        // verify DAO calls
        verify(employeeRepository, times(1)).findById(employee.getId());
        verify(employeeRepository, times(1)).save(employee);
    }

    // JUnit test for save Employee method that throws exception
    @Test
    public void givenExistingId_whenSaveEmployee_thanThrowsException() {
        // given - precondition or state
        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));
        //when(employeeRepository.save(employee)).thenReturn(employee);

        // when - action or behavior that we are going test
        assertThrows(ResourceRunTimeException.class, () ->  {
            employeeService.saveEmployee(employee);
        });

        // than - verify the output
        verify(employeeRepository, never()).save(any(Employee.class));
    }

    // JUnit test for findAllEmployees
    @Test
    public void givenEmployeesList_whenFindAllEmployees_thanReturnEmployeesList() {
        // given - precondition or state
        given(employeeRepository.findAll()).willReturn(List.of(employee, employee));

        // when - action or behavior that we are going test
        List<Employee> employeeList = employeeService.findAllEmployees();

        // than - verify the output
        assertNotNull(employeeList);
        assertEquals(2, employeeList.size(), "Number of records should be 2");
    }

    // JUnit test for findAllEmployees but negative scenario
    @Test
    public void givenEmptyEmployeesList_whenFindAllEmployees_thanReturnEmptyEmployeesList() {
        // given - precondition or state
        given(employeeRepository.findAll()).willReturn(Collections.emptyList());
        // when - action or behavior that we are going test
        List<Employee> employeeList = employeeService.findAllEmployees();
        // than - verify the output
        assertEquals(0, employeeList.size());
    }

    // JUnit test for update Employee
    @Test
    public void givenEmployeeObject_whenUpdateEmployee_thanReturnUpdatedEmployee() {
        // given - precondition or state
        given(employeeRepository.save(employee)).willReturn(employee);
        // when - action or behavior that we are going test
        employee.setLastName("srkoman");
        Employee updatedEmployee = employeeService.updateEmployee(employee);
        // than - verify the output
        assertSame("srkoman", employee.getLastName());
    }

    // JUnit test for delete Employee
    @Test
    public void givenEmployeeId_whenDeleteEmployee_thanNoting() {
        // given - precondition or state
        long employeeId = 1L;
        willDoNothing().given(employeeRepository).deleteById(employeeId);

        // when - action or behavior that we are going test
        employeeService.deleteEmploy(employeeId);

        // than - verify the output
        verify(employeeRepository, times(1)).deleteById(employeeId);
    }

}
