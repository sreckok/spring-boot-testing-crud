package com.example.springboottesting.repository;

import com.example.springboottesting.model.Employee;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    @Order(1)
    public void givenEmployeeObject_whenSave_thanReturnSavedEmployee() {

        // given
        Employee employee = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();

        // when action that we are testing
        Employee savedEmployee = employeeRepository.save(employee);

        // than check the output
//        assertThat(savedEmployee).isNotNull();
//        assertThat(savedEmployee.getId()).isGreaterThan(0);
        assertNotNull(savedEmployee);
        assertTrue(savedEmployee.getId() > 0);
    }

    // JUnit test for
    @Test
    public void givenEmployeesList_whenFindAll_thanEmployeesList() {
        // given - precondition or state
        Employee employee1 = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();

        Employee employee2 = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();

        employeeRepository.save(employee1);
        employeeRepository.save(employee2);

        // when - action or behavior that we are going test
        List<Employee> employeeList = employeeRepository.findAll();

        // than - verify the output
//        assertThat(employeeList).isNotNull();
//        assertThat(employeeList.size()).isEqualTo(2);
        assertNotNull(employeeList);
        assertEquals(2, employeeList.size());
    }


    // JUnit test for get Employee by id
    @DisplayName("JUnit test for get Employee by id")
    @Test
    public void givenEmployeeObject_whenFindById_thanReturnEmployeeObject() {
        // given - precondition or state
        Employee employee = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();
        Employee insertedEmployee = employeeRepository.save(employee);

        // when - action or behavior that we are going test
        Employee employeeFromDb = employeeRepository.findById(insertedEmployee.getId()).get();

        // than - verify the output
//        assertThat(employeeFromDb).isNotNull();
        assertNotNull(employeeFromDb);
    }

    // JUnit test for find Employee by email address
    @Test
    public void givenEmployeeObject_whenFindByEmail_thanReturnEmployeeObject() {
        // given - precondition or state
        Employee employee = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();

        employeeRepository.save(employee);

        // when - action or behavior that we are going test
        Employee employeeFromDb = employeeRepository.findByEmail("john@doe.com");

        // than - verify the output
//        assertThat(employeeFromDb).isNotNull();
        assertNotNull(employeeFromDb);
    }

    // JUnit test for update employee
    @Test
    @DisplayName("JUnit test for update employee")
    public void givenEmployeeObject_whenUpdateEmployee_thanReturnUpdatedEmployee() {
        // given - precondition or state
        Employee employee = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();
        employeeRepository.save(employee);

        // when - action or behavior that we are going test
        Employee employeeFromDb = employeeRepository.findByEmail("john@doe.com");
        employeeFromDb.setFirstName("srkoman");
        employeeFromDb.setLastName("srk");
        Employee updatedEmployee = employeeRepository.save(employeeFromDb);

        // than - verify the output
//        assertThat(updatedEmployee.getFirstName()).isEqualTo("srkoman");
//        assertThat(updatedEmployee.getLastName()).isEqualTo("srk");
        assertSame("srkoman", updatedEmployee.getFirstName());
        assertSame("srk", updatedEmployee.getLastName());
    }

    // JUnit test for delete employee
    @Test
    public void givenEmpoyeeObject_whenDelete_thanRemoveEmployee() {
        // given - precondition or state
        Employee employee = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();
        Employee employeeSavedInDb = employeeRepository.save(employee);

        // when - action or behavior that we are going test
        employeeRepository.delete(employeeSavedInDb);
        List<Employee> employeeList = employeeRepository.findAll(); // employeeRepository.findById(employee.getId))

        // than - verify the output
//        assertThat(employeeList).isEmpty();
        assertTrue(employeeList.isEmpty());
    }

    // JUnit test for custom jpql query
    @Test
    public void givenEmployeeObject_whenFindByJPQL_thanReturnEmployee() {
        // given - precondition or state
        Employee employee = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();
        employeeRepository.save(employee);

        // when - action or behavior that we are going test
        Employee employeeFromDb = employeeRepository.findByJPQL("John", "Doe");

        // than - verify the output
//        assertThat(employeeFromDb).isNotNull();
//        assertThat(employeeFromDb.getLastName()).isEqualTo("Doe");
        assertNotNull(employeeFromDb);
        assertEquals("Doe", employeeFromDb.getLastName());
    }

    // JUnit test for native sql query
    @Test
    public void givenEmployeeObject_whenFindByNativeSql_thanReturnEmployee() {
        // given - precondition or state
        Employee employee = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@doe.com")
                .build();
        employeeRepository.save(employee);

        // when - action or behavior that we are going test
        Employee employeeFromDb = employeeRepository.findByNativeSql("John", "Doe");

        // than - verify the output
//        assertThat(employeeFromDb).isNotNull();
//        assertThat(employeeFromDb.getLastName()).isEqualTo("Doe");
        assertNotNull(employeeFromDb);
        assertEquals("Doe", employeeFromDb.getLastName());
    }
}
